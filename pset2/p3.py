"""
balance - the outstanding balance on the credit card. Type: float.
annualInterestRate - annual interest rate as a decimal. Type: float.

A program that calculates the minimum fixed monthly payment needed in
order pay off a credit card balance within 12 months. A fixed monthly payment,
in this case, is a single number which does not change each month, but instead is
a constant amount that will be paid each month.
This program uses bisection search algorithm, and unlike p2.py the balance can't
go negative and the answer is approximated to the hundreds(in fact , it is more accurate than p2.py answer).
"""
#The grader doesn't wan't you to submit input statements.
balance = float(input("Type your balance."))
annualInterestRate = float(input("Type your annual interest rate as a decimal.")).
monthlyInterestRate = annualInterestRate/12
savedBalance = balance
low = balance/12
high =  (balance*(1+monthlyInterestRate)**12)/12
minimumMonthlyPayment = 0
unpaidBalance = 0
while abs(balance) > 0.01:
    balance = savedBalance
    unpaidBalance = 0
    minimumMonthlyPayment = (high+low)/2
    for month in range(12):
        unpaidBalance = balance - minimumMonthlyPayment
        balance = unpaidBalance + (monthlyInterestRate * unpaidBalance)
    if  balance > 0:
        low = minimumMonthlyPayment
    else:
        high = minimumMonthlyPayment
    minimumMonthlyPayment = (high+low)/2
print("Lowest payment is: " + str(round(minimumMonthlyPayment,2)))