"""
balance - the outstanding balance on the credit card. Type: float.
annualInterestRate - annual interest rate as a decimal. Type: float.

A program that calculates the minimum fixed monthly payment needed in
order pay off a credit card balance within 12 months. A fixed monthly payment,
in this case, is a single number which does not change each month, but instead is
a constant amount that will be paid each month.

Assume that the interest is compounded monthly according to the balance at the end
of the month (after the payment for that month is made). The monthly payment must 
be a multiple of $10 and is the same for all months. Notice that it is possible for
the balance to become negative using this payment scheme, which is okay for this problem.
"""
#The grader doesn't wan't you to submit input statements.
balance = float(input("Type your balance."))
annualInterestRate = float(input("Type your annual interest rate as a decimal.")).
monthlyPaymentRate = float(input("Type your minimum monthly payment rate as a decimal"))
monthlyInterestRate= annualInterestRate / 12.0
unpaidBalance = 1
minimumPayment = 0
savedbalance = balance
while unpaidBalance > 0:
    balance = savedbalance
    minimumPayment+= 10
    month = 1
    while month < 13:
        unpaidBalance = balance - minimumPayment
        interest = unpaidBalance * (annualInterestRate/12)
        balance = unpaidBalance + interest
        month+= 1
print("Lowest Payment: " + str(minimumPayment))