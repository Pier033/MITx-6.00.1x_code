"""
balance - the outstanding balance on the credit card. Type: float.
annualInterestRate - annual interest rate as a decimal. Type: float
monthlyPaymentRate - minimum monthly payment rate as a decimal. Type: float.

A program to calculate the credit card balance after one year if a person only pays
the minimum monthly payment required by the credit card company each month.

"""
#The grader doesn't wan't you to submit input statements.
balance = float(input("Type your balance."))
annualInterestRate = float(input("Type your annual interest rate as a decimal.")).
monthlyPaymentRate = float(input("Type your minimum monthly payment rate as a decimal"))
monthlyInterestRate= annualInterestRate / 12.0
for month in range(12):
    minimumMonthlyPayment = monthlyPaymentRate * balance
    unpaidBalance = balance - minimumMonthlyPayment
    balance = unpaidBalance + (monthlyInterestRate * unpaidBalance)
print("Remaining balance: " + str(round(balance,2)))