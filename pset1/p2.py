"""
s: lower case string

This program checks how many times the substring 'bob' occurs in s, and prints it.

"""

s = input("Type something to check how many times 'bob' occurs in it.")   #The grader doesn't want you to set your own input values when you submit your code.
counter = 0 
for i in range(len(s)):
    check = s[i:i+3]
    if check == "bob":
        counter += 1
print("Number of times bob occurs is: " + str(counter))