"""
s: string

This program finds the the longest substring in s whose characters are in alphabetical order.

"""

s = input("Type a string to find its longest substring whose characters are in alphabetical order.")  #The grader doesn't want you to submit input statements.
check = ""
savedString = check
for i in range(len(s)):
    if s[i] > s[i-1] or len(check) == 0 or s[i] == s[i-1]:
        check += s[i]
        if len(check) > len(savedString):
            savedString = check
    else:
        check = s[i]
print("Longest substring in alphabetical order is: " + str(savedString)) 